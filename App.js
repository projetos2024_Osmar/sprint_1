import { NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MenuInicial from './src/menuInicial';
import Login from './src/login'
import Home from './src/home'
import Cadastro from './src/cadastro';

const Stack = createStackNavigator();

export default function StackRoutes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='MenuInicial'>
        <Stack.Screen name='MenuInicial' component={MenuInicial} options={{headerShown: false}} />
        <Stack.Screen name='Login' component={Login} options={{headerShadowVisible: false, headerTransparent: true, title: ''}} />
        <Stack.Screen name='Home' component={Home} options={{headerShadowVisible: false, headerTransparent: true, title: ''}} />
        <Stack.Screen name='Cadastro' component={Cadastro} options={{headerShadowVisible: false, headerTransparent: true, title: ''}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}