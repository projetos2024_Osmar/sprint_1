import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Alert } from "react-native";

// Importando as funções de login do backend
import sheets from "./axios/axios";

// Componente de botão personalizado
import LButton from "./components/LButton";

// Componente de tela de login
const Login = ({ navigation }) => {
  // Estados para armazenar CPF e senha inseridos pelo usuário
  const [cpf, setCpf] = useState("");
  const [senha, setSenha] = useState("");

  // Função para lidar com a submissão do formulário de login
  const handleLoginSubmit = async () => {
    try {
      // Verifica se os campos de CPF e senha foram preenchidos
      if (cpf !== "" && senha !== "") {
        // Chama a função de login do backend passando CPF e senha
        const response = await sheets.login({
          cpf,
          senha,
        });

        // Verificar a resposta da API
        if (response.status === 200) {
          // Se o login for bem-sucedido, exibe um alerta e navega para a tela principal
          alert("Login efetuado com sucesso");
          navigation.navigate("Home");
        } 
      } else {
        // Se os campos não estiverem preenchidos, exibe um alerta
        alert("Preencha os campos para entrar");
      }
    } catch (error) {
      // Se ocorrer algum erro durante o login, exibe um alerta com a mensagem de erro
      alert("Erro ao realizar login, por favor, tente novamente: " + error.response.data.message);
    }
  };

  return (
    <View style={styles.container}>
      {/* Título da tela de login */}
      <Text style={styles.titleText}>ENTRAR</Text>
      <View style={styles.form}>
        {/* Campo de entrada para o CPF */}
        <Text style={styles.textoInput}>CPF</Text>
        <TextInput
          style={styles.input}
          value={cpf}
          onChangeText={(text) =>
            // Formatação do CPF para permitir apenas números e manter no formato correto
            setCpf(text.replace(/[^0-9]/g, "").substring(0, 11))
          }
          maxLength={11}
          keyboardType="numeric"
        />
        {/* Campo de entrada para a senha */}
        <Text style={styles.textoInput}>Senha</Text>
        <TextInput
          style={styles.input}
          value={senha}
          onChangeText={(text) => setSenha(text)}
          secureTextEntry={true}
        />
      </View>
      {/* Botão de login */}
      <LButton
        backgroundColor={"#751319"}
        height={60}
        onPress={handleLoginSubmit}
      >
        Entrar
      </LButton>
    </View>
  );
};

// Estilos para os elementos da tela de login
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F9F5EB",
    height: "100%",
    width: "100%",
  },
  titleText: {
    fontSize: 28,
    marginTop: 52,
  },
  form: {
    alignItems: "center",
    marginTop: 100,
    marginBottom: 55,
  },
  input: {
    backgroundColor: "#F9F5EB",
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: "#000",
    width: 310,
    height: 48,
    marginBottom: 35,
    paddingLeft: 6,
  },
  textoInput: {
    alignSelf: "flex-start",
    marginLeft: 2,
    fontSize: 16,
  },
});

export default Login;
