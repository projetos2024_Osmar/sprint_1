import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, ScrollView, StyleSheet } from 'react-native';

const Home = () => {
  // Estados para o status, capacidade e mesas reservadas
  const [status, setStatus] = useState('');
  const [capacidade, setCapacidade] = useState('');

  //Cadastrar mesa
  const [reservedTables, setReservedTables] = useState([]);

  //Mostrar e Ocultar lista de mesas cadastradas
  const [showReservedTables, setShowReservedTables] = useState(false);
  
  // Estados para edição
  const [editIndex, setEditIndex] = useState(null);
  const [editStatus, setEditStatus] = useState('');
  const [editCapacidade, setEditCapacidade] = useState('');

  // Função para reservar uma mesa
  const reserveTable = () => {
    // Verifica se o status e a capacidade estão preenchidos
    if (status.trim() !== '' && capacidade.trim() !== '') {
      // Adiciona o status e a capacidade à lista de mesas reservadas
      const reservation = {
        status: status,
        capacidade: capacidade,
      };

      setReservedTables([...reservedTables, reservation]);

      // Limpa os campos de entrada
      setStatus('');
      setCapacidade('');
    } else {
      alert('Por favor, insira o status e a capacidade');
    }
  };

  // Função para excluir uma reserva de mesa
  const deleteReservation = (index) => {
    const updatedReservedTables = [...reservedTables];
    updatedReservedTables.splice(index, 1);
    setReservedTables(updatedReservedTables);
  };

  // Função para editar uma reserva de mesa
  const editReservation = (index) => {
    setEditIndex(index);
    setEditStatus(reservedTables[index].status);
    setEditCapacidade(reservedTables[index].capacidade);
  };

  // Função para salvar a edição de uma reserva de mesa
  const saveEdit = () => {
    const updatedReservedTables = [...reservedTables];
    updatedReservedTables[editIndex] = {
      status: editStatus,
      capacidade: editCapacidade,
    };
    setReservedTables(updatedReservedTables);
    setEditIndex(null);
    setEditStatus('');
    setEditCapacidade('');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Cadastro de Mesas</Text>
      <View style={styles.form}>
        <Text style={styles.label}>Status:</Text>
        <View style={styles.checkboxContainer}>
          <TouchableOpacity
            style={[styles.checkbox, status === 'Disponível' && styles.checked]}
            onPress={() => setStatus('Disponível')}
          />
          <Text style={styles.checkboxText}>Disponível</Text>
          <TouchableOpacity
            style={[styles.checkbox, status === 'Ocupado' && styles.checked]}
            onPress={() => setStatus('Ocupado')}
          />
          <Text style={styles.checkboxText}>Ocupado</Text>
        </View>
        <Text style={styles.label}>Capacidade:</Text>
        <TextInput
          style={styles.input}
          placeholder="Digite a capacidade"
          keyboardType="numeric"
          value={capacidade}
          onChangeText={(text) => setCapacidade(text)}
        />
        <TouchableOpacity style={styles.buttonCadastro} onPress={reserveTable}>
          <Text style={styles.buttonText}>Cadastrar Mesa</Text>
        </TouchableOpacity>
        
        <TouchableOpacity 
          style={styles.button} 
          onPress={() => setShowReservedTables(!showReservedTables)}
        >
          <Text style={styles.buttonText}>
            {showReservedTables ? 'Esconder Mesas' : 'Mostrar Mesas'}
          </Text>
        </TouchableOpacity>
      </View>
      
      {showReservedTables && (
        <View style={styles.reservedTables}>
          <Text style={styles.reservedTablesTitle}>Mesas Cadastradas:</Text>
          <ScrollView>
            {reservedTables.map((reservation, index) => (
              <View key={index} style={styles.reservedTable}>
                {editIndex === index ? (
                  <>
                    <View style={styles.checkboxContainer}>
                      <TouchableOpacity
                        style={[styles.checkbox, editStatus === 'Disponível' && styles.checked]}
                        onPress={() => setEditStatus('Disponível')}
                      />
                      <Text style={styles.checkboxText}>Disponível</Text>
                      <TouchableOpacity
                        style={[styles.checkbox, editStatus === 'Ocupado' && styles.checked]}
                        onPress={() => setEditStatus('Ocupado')}
                      />
                      <Text style={styles.checkboxText}>Ocupado</Text>
                    </View>
                    <TextInput
                      style={styles.input}
                      placeholder="Capacidade"
                      keyboardType="numeric"
                      value={editCapacidade}
                      onChangeText={(text) => setEditCapacidade(text)}
                    />
                    <TouchableOpacity style={styles.saveButton} onPress={saveEdit}>
                      <Text style={styles.buttonText}>Salvar</Text>
                    </TouchableOpacity>
                  </>
                ) : (
                  <View>
                    <Text>Mesa {index + 1}:</Text>
                    <Text>Status: {reservation.status}</Text>
                    <Text>Capacidade: {reservation.capacidade}</Text>
                  </View>
                )}
                <View style={styles.buttonContainer}>
                  <TouchableOpacity 
                    style={styles.editButton} 
                    onPress={() => editReservation(index)}
                  >
                    <Text style={styles.buttonText}>Editar</Text>
                  </TouchableOpacity>
                  <TouchableOpacity 
                    style={styles.deleteButton} 
                    onPress={() => deleteReservation(index)}
                  >
                    <Text style={styles.buttonText}>Excluir</Text>
                  </TouchableOpacity>
                </View>
              </View>
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "F9F5EB",
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  form: {
    width: '100%',
  },
  label: {
    fontSize: 18,
    marginBottom: 10,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
    fontSize: 16,
    height: 40,
  },
  button: {
    backgroundColor: '#751319',
    paddingVertical: 15,
    borderRadius: 5,
    alignItems: 'center',
    marginBottom: 10,
  },
  buttonCadastro: {
    backgroundColor: '#D47608',
    paddingVertical: 15,
    borderRadius: 5,
    alignItems: 'center',
    marginBottom: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  reservedTables: {
    marginTop: 20,
    flex: 1,
  },
  reservedTablesTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  reservedTable: {
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#ccc',
    padding: 10,
    borderRadius: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  editButton: {
    backgroundColor: '#D47608',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  deleteButton: {
    backgroundColor: '#751319',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginLeft: 10,
  },
  saveButton: {
    backgroundColor: 'green',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  checkbox: {
    width: 20,
    height: 20,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#000',
    marginRight: 10,
  },
  checked: {
    backgroundColor: '#007bff',
  },
  checkboxText: {
    fontSize: 16,
    marginRight: 10,
  },
});

export default Home;
