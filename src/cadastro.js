// Importações necessárias do React Native e do arquivo de requisições Axios
import React, { useState } from "react";
import { View, Text, StyleSheet, TextInput, Alert } from "react-native";
import sheets from "./axios/axios"; // Importa o objeto que contém as funções Axios
import LButton from "./components/LButton"; // Importa o componente de botão personalizado

// Definição do componente de cadastro
const Cadastro = ({ navigation }) => {
  // Definição dos estados para os campos do formulário
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [nome, setNome] = useState("");
  const [cpf, setCpf] = useState("");
  const [telefone, setTelefone] = useState("");
  const [confirmarSenha, setConfirmarSenha] = useState("");

  // Função para lidar com o envio do formulário de cadastro
  const handleCadastroSubmit = async () => {
    try {
      // Verifica se todos os campos estão preenchidos
      if (nome && email && senha && confirmarSenha && cpf && telefone) {
        // Verifica se as senhas coincidem
        if (senha !== confirmarSenha) {
          alert("As senhas informadas não coincidem");
        } else {
          // Envia a requisição de cadastro para a API utilizando Axios
          const response = await sheets.cadastro({
            nome,
            email,
            cpf,
            telefone,
            senha,
          });
          // Verifica se o cadastro foi realizado com sucesso
          if (response.status === 200) {
            alert("Cadastro realizado com sucesso");
            navigation.navigate("Home"); // Navega para a tela "Home"
          } else {
            alert("Erro ao cadastrar cliente");
          }
        }
      } else {
        alert("Preencha todos os campos");
      }
    } catch (error) {
      // Trata erros caso ocorra algum problema na requisição
      console.error("Erro ao cadastrar cliente:", error);
      alert("Erro ao cadastrar cliente, por favor, tente novamente");
    }
  };

  // Estrutura do componente de cadastro
  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>CADASTRAR</Text>
      <View style={styles.form}>
        {/* Campos do formulário */}
        <Text style={styles.textoInput}>Nome</Text>
        <TextInput
          style={styles.input}
          value={nome}
          onChangeText={(text) => setNome(text)}
        />

        <Text style={styles.textoInput}>Telefone</Text>
        <TextInput
          style={styles.input}
          value={telefone}
          keyboardType="numeric"
          maxLength={11}
          onChangeText={(number) => setTelefone(number)}
        />

        <Text style={styles.textoInput}>CPF</Text>
        <TextInput
          style={styles.input}
          value={cpf}
          keyboardType="numeric"
          maxLength={11}
          onChangeText={(text) => setCpf(text.replace(/[^0-9]/g, ""))}
        />

        <Text style={styles.textoInput}>E-mail</Text>
        <TextInput
          style={styles.input}
          value={email}
          onChangeText={(text) => setEmail(text)}
        />

        <Text style={styles.textoInput}>Senha</Text>
        <TextInput
          style={styles.input}
          value={senha}
          onChangeText={(text) => setSenha(text)}
          secureTextEntry={true}
        />

        <Text style={styles.textoInput}>Confirmar Senha</Text>
        <TextInput
          style={styles.input}
          value={confirmarSenha}
          onChangeText={(text) => setConfirmarSenha(text)}
          secureTextEntry={true}
        />
      </View>
      {/* Botão de cadastro */}
      <LButton
        backgroundColor={"#D47608"}
        height={60}
        onPress={handleCadastroSubmit}
      >
        Cadastrar
      </LButton>
    </View>
  );
};

// Estilos do componente
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F9F5EB",
    height: "100%",
    width: "100%",
  },
  titleText: {
    fontSize: 28,
    marginTop: 52,
  },
  form: {
    alignItems: "center",
    marginTop: "13%",
    marginBottom: 44,
  },
  input: {
    backgroundColor: "#F9F5EB",
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: "#000",
    width: 310,
    height: 48,
    marginBottom: 14,
    paddingLeft: 6,
  },
  textoInput: {
    alignSelf: "flex-start",
    marginLeft: 2,
    fontSize: 16,
  },
});

export default Cadastro; // Exporta o componente Cadastro
