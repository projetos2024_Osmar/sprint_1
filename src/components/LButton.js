import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';

export default function LButton(props) {
    // Valores padrão para estilização
    const defaultHeight = 66;
    const defaultWidth = 325;
    const defaultBackgroundColor = '#0d0d0d';
    const defaultColor = '#fff';
    const defaultFontSize = 26;
    const defaultMarginBottom = 3;

    // useState para armazenar valores passados por props
    const [customHeight] = useState(props.height || defaultHeight);
    const [customWidth] = useState(props.width || defaultWidth);
    const [customBackgroundColor] = useState(props.backgroundColor || defaultBackgroundColor);
    const [customColor] = useState(props.color || defaultColor);
    const [customFontSize] = useState(props.fontSize || defaultFontSize);
    const [customMarginBottom] = useState(props.marginBottom || defaultMarginBottom);

    return (
        <TouchableOpacity
            style={{
                width: customWidth,
                height: customHeight,
                backgroundColor: customBackgroundColor,
                marginBottom: customMarginBottom,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 16,
            }}
            onPress={props.onPress}
        >
            <Text style={{ color: customColor, fontSize: customFontSize }}>
                {props.children}
            </Text>
        </TouchableOpacity>
    );
}
