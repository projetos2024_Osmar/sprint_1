import axios from "axios"
import Cadastro from "../cadastro";

const api = axios.create({
    baseURL: "http://10.89.234.144:5000/restaurante",
    headers: {
        'Accept': 'application/json',
    },
});

const sheets = {
    login:(user) => api.post("/login", user),
    cadastro:(user) => api.post("/cadastro", user)

}

export default sheets;